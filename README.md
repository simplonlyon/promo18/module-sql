# module-sql

## Commandes SQL

### Afficher la liste des bases de données auxquelles mon utilisateur a accès : 
```sql
SHOW DATABASES;
```

### Sélectionner une base pour lancer les commandes/requêtes :
```sql
USE nom_de_la_base;
```

### Afficher les tables dans ma base (MySQL/MariaDB)
```sql
SHOW TABLES;
```

### Afficher la structure d'une table (la liste de ses champs, les clés, etc)
```sql
DESCRIBE nom_de_la_table;
```

### Afficher la requête SQL qui permet de créer une table
```sql
SHOW CREATE TABLE nom_de_la_table;
```

### Compter combien il y a de lignes dans ma table product
```sql
SELECT COUNT(*) FROM product;
```

### Compter combien il y a d'articles pour homme dans ma table product
```sql
SELECT COUNT(*) FROM product WHERE gender = 'Homme';
```

### Compter combien il y a d'articles par genre dans ma table product
```sql
SELECT gender, COUNT(*) FROM product GROUP BY gender;
```

### Afficher le prix minimum regroupé par couleur
```sql
SELECT color, MIN(price) FROM product GROUP BY color;
```
#### En renommant la colonne MIN(price) EN PRIX_MINIMUM
```sql
SELECT color, MIN(price) AS PRIX_MINIMUM FROM product GROUP BY color;
```

### Afficher toutes les commandes qui datent de juin 2022 (3 méthodes)
#### Méthode 1 : en utilisant les fonctions de date
```sql
SELECT * FROM order_table WHERE MONTH(date) = 6 AND YEAR(date) = 2022;
```
#### Méthode 2 : avec BETWEEN
```sql
SELECT * FROM order_table WHERE date BETWEEN '2022-06-01' AND '2022-06-30';
```
#### Méthode 3 : avec LIKE (seulement sur MySQL/MariaDB)
```sql
SELECT * FROM order_table WHERE date LIKE '2022-06%';
```

### Afficher pour la commande n°18 la liste des produits commandés (leur label et leur genre), la quantité commandée, le prix à l'unité de chaque produit dans la commande, et la date de la commande
```sql
SELECT
    ot.id, ot.date, p.label, p.gender, i.quantity, i.price
    FROM order_table ot
        JOIN item i
            ON i.order_id = ot.id
        JOIN product p
            ON p.id = i.product_id
    WHERE ot.id = 18;
```

### Afficher le prix total payé par le client par commande
```sql
SELECT
    order_id, SUM(price*quantity) AS PRICE_PAID
    FROM item
    GROUP BY order_id
    ORDER BY order_id;
```

### Afficher le chiffre d'affaires du site, par mois
| ANNEE|MOIS| CA     |
| 2022 | 06 | 154,14 |
```sql
SELECT
    YEAR(ot.date) AS ANNEE, MONTH(ot.date) AS MOIS, SUM(price*quantity) AS CA
    FROM item i
        JOIN order_table ot
            ON ot.id = i.order_id
    GROUP BY ANNEE, MOIS
    ORDER BY ANNEE, MOIS;
```

### Afficher les 10 dernières commandes
```sql
SELECT *
    FROM order_table
    ORDER BY id DESC
    LIMIT 10;
```
#### Afficher les 15 suivantes
```sql
SELECT *
    FROM order_table
    ORDER BY id DESC
    LIMIT 10, 15;
```