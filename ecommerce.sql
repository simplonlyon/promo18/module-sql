-- Active: 1649376266639@@127.0.0.1@3306@p18_ecommerce
-- MySQL dump 10.19  Distrib 10.3.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: g4_ecommerce
-- ------------------------------------------------------
-- Server version	10.3.34-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKda8tuywtf0gb6sedwk7la1pgi` (`user_id`),
  CONSTRAINT `FKda8tuywtf0gb6sedwk7la1pgi` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'Antibes','France','Domicile','15 Passage La Boétie','65643',1),(2,'Chambéry','France','Travail','7933 Allée, Voie de Nesle','71587',1),(3,'Vitry-sur-Seine','France','Domicile','927 Boulevard du Havre','18456',2),(4,'Tours','France','Travail','218 Rue de la Bûcherie','89525',2),(5,'Le Tampon','France','Domicile','1575 Avenue Adolphe Mille','52867',3),(6,'Montreuil','France','Travail','262 Avenue des Francs-Bourgeois','82673',3),(7,'Reims','France','Domicile','947 Place Delesseux','85091',4),(8,'Hyères','France','Travail','5 Place de Provence','26022',4),(9,'Cayenne','France','Domicile','489 Rue des Lombards','07764',5),(10,'Lyon','France','Travail','1 Boulevard de la Pompe','36721',5),(11,'Hyères','France','Domicile','68 Allée, Voie Pierre Charron','83432',6),(12,'Le Tampon','France','Travail','5 Place de l\'Abbaye','96313',6),(13,'Saint-Quentin','France','Domicile','907 Quai des Francs-Bourgeois','70216',7),(14,'Limoges','France','Travail','101 Quai Saint-Jacques','54660',7);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design`
--

DROP TABLE IF EXISTS `design`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `design`
--

LOCK TABLES `design` WRITE;
/*!40000 ALTER TABLE `design` DISABLE KEYS */;
INSERT INTO `design` VALUES (1,'Ours.png','Ours'),(2,'Epée.png','Epée'),(3,'Ballon.png','Ballon'),(4,'Château.png','Château'),(5,'Nuage.png','Nuage'),(6,'Extraterrestre.png','Extraterrestre'),(7,'Guitare.png','Guitare');
/*!40000 ALTER TABLE `design` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` float NOT NULL,
  `quantity` int(11) NOT NULL,
  `design_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKeqk5oh8xarrfrhbi370w2qjts` (`design_id`),
  KEY `FKifecq4kxptn4b9bvahbsiyr36` (`order_id`),
  KEY `FKd1g72rrhgq1sf7m4uwfvuhlhe` (`product_id`),
  KEY `FK53f45gtwjfjyt56gc4dbi5b3s` (`size_id`),
  CONSTRAINT `FK53f45gtwjfjyt56gc4dbi5b3s` FOREIGN KEY (`size_id`) REFERENCES `size` (`id`),
  CONSTRAINT `FKd1g72rrhgq1sf7m4uwfvuhlhe` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FKeqk5oh8xarrfrhbi370w2qjts` FOREIGN KEY (`design_id`) REFERENCES `design` (`id`),
  CONSTRAINT `FKifecq4kxptn4b9bvahbsiyr36` FOREIGN KEY (`order_id`) REFERENCES `order_table` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,22,2,2,1,10,3),(2,16.99,3,2,1,1,6),(3,18.49,1,7,2,12,6),(4,16.82,2,5,2,20,1),(5,15.23,2,5,3,12,4),(6,21.93,1,6,4,5,3),(7,16.34,2,7,4,18,1),(8,16.9,3,2,5,20,5),(9,19.46,3,1,5,10,4),(10,18.39,3,5,5,5,5),(11,22.34,2,4,6,18,5),(12,15.7,2,1,6,15,5),(13,23.46,3,2,6,2,4),(14,22.65,3,6,7,17,2),(15,16.44,1,6,7,3,3),(16,16.92,1,2,7,9,6),(17,15.8,1,2,8,18,5),(18,21.25,3,3,8,9,3),(19,20.09,2,2,8,6,1),(20,26.06,3,6,8,2,5),(21,19.79,1,6,9,9,5),(22,20,2,5,9,13,5),(23,15.31,1,2,9,15,5),(24,21.24,3,3,9,6,1),(25,17.67,3,4,10,6,5),(26,21.26,2,3,10,10,4),(27,17.77,3,5,11,3,6),(28,19.24,1,2,11,11,5),(29,17.24,1,3,11,20,1),(30,20.83,2,5,11,4,4),(31,22.77,3,4,12,10,3),(32,22.36,3,7,12,3,2),(33,16.91,3,6,13,12,1),(34,20.35,1,7,13,16,6),(35,17.21,3,6,14,18,6),(36,16.98,1,6,14,11,4),(37,23.3,3,1,15,15,5),(38,19.21,1,7,16,17,3),(39,18.12,2,2,17,5,5),(40,19.99,2,4,17,17,3),(41,22.43,1,6,18,2,2),(42,25.48,3,6,18,5,6),(43,15.95,1,4,18,18,4),(44,15.49,3,3,18,5,1),(45,23.7,2,5,19,8,1),(46,18.45,3,4,19,14,2),(47,23.95,1,6,20,14,6),(48,20.81,3,7,21,14,6),(49,24.81,1,1,21,6,5),(50,15.54,2,4,22,8,4),(51,20.1,1,1,22,10,2),(52,16.94,2,2,22,3,6),(53,25.58,1,1,22,17,5),(54,18.92,3,2,23,1,1),(55,22.62,1,4,23,8,4),(56,18.03,3,4,24,10,5),(57,25.26,3,7,25,10,2),(58,21.52,3,3,25,7,2),(59,24.28,2,3,25,3,2),(60,23.4,2,6,25,18,1),(61,21.57,3,1,26,8,2),(62,26.45,3,6,26,12,1),(63,16.9,1,7,27,1,6),(64,24.29,1,3,27,1,2),(65,17.58,3,4,27,2,1),(66,19.84,2,2,28,9,3),(67,18,2,7,29,5,1),(68,21.93,1,1,29,19,4),(69,20.96,3,2,29,6,6),(70,21.45,1,3,30,20,3),(71,18.46,2,4,30,19,4),(72,16.03,1,6,30,15,1),(73,25.55,1,2,30,16,2),(74,15.99,2,1,31,9,5),(75,26.26,2,7,31,15,3),(76,19.53,1,5,31,11,5),(77,19.1,2,4,32,14,6),(78,25.27,3,5,32,13,1),(79,24.5,3,5,32,8,2),(80,17.28,3,4,33,5,4);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_table`
--

DROP TABLE IF EXISTS `order_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `payment` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKan38srg49qpanv9u1hskl9m7p` (`address_id`),
  KEY `FKnmdjo6oaf01ow2reubtrhl6ev` (`user_id`),
  CONSTRAINT `FKan38srg49qpanv9u1hskl9m7p` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FKnmdjo6oaf01ow2reubtrhl6ev` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_table`
--

LOCK TABLES `order_table` WRITE;
/*!40000 ALTER TABLE `order_table` DISABLE KEYS */;
INSERT INTO `order_table` VALUES (1,'2022-04-06','CB','Expédiée',1,1),(2,'2022-03-19','CB','Expédiée',2,1),(3,'2022-02-09','CB','Expédiée',1,1),(4,'2022-03-23','CB','Expédiée',2,1),(5,'2022-06-02','CB','Expédiée',4,2),(6,'2022-02-06','CB','Expédiée',3,2),(7,'2021-11-02','CB','Expédiée',4,2),(8,'2022-03-17','CB','Expédiée',4,2),(9,'2022-06-17','CB','Expédiée',4,2),(10,'2022-01-14','CB','Expédiée',6,3),(11,'2022-01-03','CB','Expédiée',5,3),(12,'2021-09-28','CB','Expédiée',5,3),(13,'2021-11-28','CB','Expédiée',6,3),(14,'2022-05-13','CB','Expédiée',8,4),(15,'2021-10-01','CB','Expédiée',8,4),(16,'2022-05-13','CB','Expédiée',8,4),(17,'2021-10-23','CB','Expédiée',8,4),(18,'2021-12-09','CB','Expédiée',7,4),(19,'2022-01-28','CB','Expédiée',9,5),(20,'2022-05-18','CB','Expédiée',10,5),(21,'2022-01-22','CB','Expédiée',9,5),(22,'2022-06-05','CB','Expédiée',12,6),(23,'2022-05-31','CB','Expédiée',12,6),(24,'2022-05-01','CB','Expédiée',12,6),(25,'2022-06-05','CB','Expédiée',11,6),(26,'2022-03-06','CB','Expédiée',12,6),(27,'2022-02-06','CB','Expédiée',12,6),(28,'2022-01-31','CB','Expédiée',12,6),(29,'2022-01-18','CB','Expédiée',12,6),(30,'2022-06-06','CB','Expédiée',14,7),(31,'2022-01-03','CB','Expédiée',14,7),(32,'2022-05-13','CB','Expédiée',13,7),(33,'2021-10-08','CB','Expédiée',13,7);
/*!40000 ALTER TABLE `order_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `reference` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Noir','Homme','ts-noir.png','T-Shirt homme de couleur noir',15.97,'TS-HOMME-NOIR'),(2,'Noir','Femme','ts-noir.png','T-Shirt femme de couleur noir',20.9,'TS-FEMME-NOIR'),(3,'Noir','Enfant','ts-noir.png','T-Shirt enfant de couleur noir',25.26,'TS-ENFANT-NOIR'),(4,'Blanc','Homme','ts-blanc.png','T-Shirt homme de couleur blanc',19.49,'TS-HOMME-BLANC'),(5,'Blanc','Femme','ts-blanc.png','T-Shirt femme de couleur blanc',16.83,'TS-FEMME-BLANC'),(6,'Blanc','Enfant','ts-blanc.png','T-Shirt enfant de couleur blanc',21.51,'TS-ENFANT-BLANC'),(7,'Bleu','Homme','ts-bleu.png','T-Shirt homme de couleur bleu',16.07,'TS-HOMME-BLEU'),(8,'Bleu','Femme','ts-bleu.png','T-Shirt femme de couleur bleu',20.89,'TS-FEMME-BLEU'),(9,'Bleu','Enfant','ts-bleu.png','T-Shirt enfant de couleur bleu',24.59,'TS-ENFANT-BLEU'),(10,'Gris','Homme','ts-gris.png','T-Shirt homme de couleur gris',19.03,'TS-HOMME-GRIS'),(11,'Gris','Femme','ts-gris.png','T-Shirt femme de couleur gris',21.18,'TS-FEMME-GRIS'),(12,'Gris','Enfant','ts-gris.png','T-Shirt enfant de couleur gris',21.08,'TS-ENFANT-GRIS'),(13,'Rouge','Homme','ts-rouge.png','T-Shirt homme de couleur rouge',22.83,'TS-HOMME-ROUGE'),(14,'Rouge','Femme','ts-rouge.png','T-Shirt femme de couleur rouge',17.94,'TS-FEMME-ROUGE'),(15,'Rouge','Enfant','ts-rouge.png','T-Shirt enfant de couleur rouge',18.82,'TS-ENFANT-ROUGE'),(16,'Vert','Homme','ts-vert.png','T-Shirt homme de couleur vert',25.31,'TS-HOMME-VERT'),(17,'Vert','Femme','ts-vert.png','T-Shirt femme de couleur vert',25.21,'TS-FEMME-VERT'),(18,'Vert','Enfant','ts-vert.png','T-Shirt enfant de couleur vert',23.66,'TS-ENFANT-VERT'),(19,'Orange','Homme','ts-orange.png','T-Shirt homme de couleur orange',21.76,'TS-HOMME-ORANGE'),(20,'Orange','Femme','ts-orange.png','T-Shirt femme de couleur orange',24.21,'TS-FEMME-ORANGE');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `size`
--

DROP TABLE IF EXISTS `size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `size`
--

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;
INSERT INTO `size` VALUES (1,'XS'),(2,'S'),(3,'M'),(4,'L'),(5,'XL'),(6,'XXL');
/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'user@test.com','user_firstname','user_lastname','$2a$12$jhMNQNMr3SfGhbk/TQOZpuu4B4a05VmT31eKfYYO3OG80CGierSK2','0757560236','ROLE_USER'),(2,'admin@test.com','admin_firstname','admin_lastname','$2a$12$63au8t4xNocML4AVIM1AQO45Ygt97r5L6uFAeI20Tplg82xQ472fW','0629560198','ROLE_ADMIN'),(3,'nicolas.breton@gmail.com','Nicolas','Breton','$2a$12$HMKibcDmvqN9Y5YhEErxMeXuvQjOSqoCEYAmWr/5pIeksnNuKKvJ2','0652558475','ROLE_USER'),(4,'louna.bourgeois@yahoo.fr','Louna','Bourgeois','$2a$12$17ltJAKIF9ITYkFgDKGekOV7hZvwlarBjglGuwTqwQkg0dq7ZeYtu','0729395351','ROLE_USER'),(5,'julien.moulin@hotmail.fr','Julien','Moulin','$2a$12$FaLVKUOpN/Fm7eZ4Zt8ePOK0grwdnclyjjO2f2H7Ylancbhm/eTiG','0668326857','ROLE_USER'),(6,'enzo.meyer@gmail.com','Enzo','Meyer','$2a$12$w9wBKKnm0tiBAcebuXy.S.ANF6s4otsKbtG2fIE.1JTzzA7qm/h9m','0676539758','ROLE_USER'),(7,'jules.rolland@hotmail.fr','Jules','Rolland','$2a$12$oMYiJ5nLpmGhl1MfuROCSeZ5Y5ljYRrRNQA7VvpGnkbmOdJ5qnPLW','0743079822','ROLE_USER');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-01 11:27:19
